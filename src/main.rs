#![deny(warnings, unused)]
#[macro_use]
extern crate log;

use std::str::FromStr;
use tokio::runtime::current_thread::Runtime;
use futures::{sync::mpsc, Stream, Future};
use gitlab::webhooks::{WebHook, IssueAction,
                       MergeRequestAction, WikiPageAction};
use clap::{App, Arg};

const DEFAULT_HTTP_PORT: u16 = 8080;

mod webserver;
mod xmpp;

fn format_webhook(wh: &WebHook, usernames: bool) -> Option<String> {
    let user_callsign = |user: &gitlab::webhooks::UserHookAttrs| if usernames { user.username.clone() } else { user.name.clone() };
    Some(match wh {
        WebHook::Push(push) => {
            let mut text =
                format!("{} pushed {} commits to {} branch {}",
                        if usernames { &push.user_username } else { &push.user_name }, push.commits.len(),
                        push.project.name, push.ref_
                );
            for commit in &push.commits {
                match commit.message.lines().nth(0) {
                    Some(subject) => {
                        text = format!("{}\n• {} <{}>", text, subject, commit.url);
                    }
                    None => {}
                }
            }
            text
        }
        WebHook::Issue(issue) => {
            // Error("invalid value: web hook, expected
            // Error(\"invalid value: hook date, expected
            // ParseError(Invalid)\", line: 0, column: 0)", line: 0,
            // column: 0)
            let action = match issue.object_attributes.action {
                Some(IssueAction::Update) =>
                    "updated",
                Some(IssueAction::Open) =>
                    "opened",
                Some(IssueAction::Close) =>
                    "closed",
                Some(IssueAction::Reopen) =>
                    "reopened",
                None =>
                    return None,
            };
            format!("{} {} issue {} in {}: {}{}",
                    user_callsign(&issue.user),
                    action,
                    issue.object_attributes.iid,
                    issue.project.name,
                    issue.object_attributes.title,
                    issue.object_attributes.url.as_ref()
                        .map(|url| format!(" <{}>", url))
                        .unwrap_or("".to_owned())
            )
        }
        WebHook::MergeRequest(merge_req) => {
            let action = match merge_req.object_attributes.action {
                Some(MergeRequestAction::Update) =>
                    "updated",
                Some(MergeRequestAction::Open) =>
                    "opened",
                Some(MergeRequestAction::Close) =>
                    "closed",
                Some(MergeRequestAction::Reopen) =>
                    "reopened",
                Some(MergeRequestAction::Merge) =>
                    "merged",
                Some(MergeRequestAction::Approved) =>
                    "approved",
                Some(MergeRequestAction::Unapproved) =>
                    "unapproved",
                None =>
                    return None,
            };
            format!("{} {} merge request {} in {}: {}{}",
                    user_callsign(&merge_req.user),
                    action,
                    merge_req.object_attributes.iid,
                    merge_req.project.name,
                    merge_req.object_attributes.title,
                    merge_req.object_attributes.url.as_ref()
                        .map(|url| format!(" <{}>", url))
                        .unwrap_or("".to_owned())
            )
        }
        WebHook::Note(note) => {
            println!("Note: {:?}", note);
            return None;
        }
        WebHook::Build(build) => {
            println!("Build: {:?}", build);
            return None;
        }
        WebHook::Pipeline(pipeline) => {
            let mr_desc = match &pipeline.merge_request {
                Some(mr_attrs) => format!("#{} {}", mr_attrs.id, mr_attrs.title),
                None => format!("{}", pipeline.object_attributes.source),
            };
            let pipeline_status = match pipeline.object_attributes.status {
                gitlab::StatusState::Created => "created",
                gitlab::StatusState::Pending => "pending",
                gitlab::StatusState::Running => "running",
                gitlab::StatusState::Success => "success",
                gitlab::StatusState::Failed => "failed",
                gitlab::StatusState::Canceled => "canceled",
                gitlab::StatusState::Skipped => "skipped",
                gitlab::StatusState::Manual => "manual",
            };
            format!("Pipeline for {} ({}) started by {} {}",
                    pipeline.project.name,
                    mr_desc,
                    user_callsign(&pipeline.user),
                    pipeline_status,
            )
        }
        WebHook::WikiPage(page) => {
            let action = match page.object_attributes.action {
                WikiPageAction::Update =>
                    "updated",
                WikiPageAction::Create =>
                    "created",
            };
            format!("{} {} {} wiki page {} <{}>",
                    user_callsign(&page.user),
                    action,
                    page.project.name,
                    page.object_attributes.title,
                    page.object_attributes.url,
            )
        }
    })
}

fn main() {
    let matches = App::new("Webhook to XMPP MUC bot")
        .version("0.0.1")
        .author("Astro <astro@spaceboyz.net>")
        .arg(Arg::with_name("JID")
             .short("j")
             .long("jid")
             .help("Jabber-ID (bare or full)")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("PASSWORD")
             .short("p")
             .long("password")
             .help("Jabber password")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("MUC")
             .short("m")
             .long("muc")
             .help("Full MUC Jabber-Id, ie. room@conference.example.com/BotNickName")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("IPV4")
             .short("4")
             .long("ipv4")
             .help("Listen on 0.0.0.0 (IPv4) for HTTP instead of :: (IPv6)"))
        .arg(Arg::with_name("PORT")
             .short("P")
             .long("port")
             .help("HTTP listening port")
             .takes_value(true))
        .arg(Arg::with_name("HEADERS")
             .short("H")
             .long("header")
             .help("Check for a header (\"X-Gitlab-Token\") or its value (\"X-Gitlab-Token: secret\")")
             .takes_value(true)
             .multiple(true))
        .arg(Arg::with_name("USERNAMES")
             .short("u")
             .long("usernames")
             .help("Use usernames instead of full names in display")
             .takes_value(false))
        .get_matches();
    let jid = matches.value_of("JID").unwrap();
    let password = matches.value_of("PASSWORD").unwrap();
    let muc = matches.value_of("MUC").unwrap();
    let ipv4 = matches.is_present("IPV4");
    let port = matches.value_of("PORT")
        .map(|port| port.parse().expect("Numeric port"))
        .unwrap_or(DEFAULT_HTTP_PORT);
    let header_checks = matches.values_of("HEADERS")
        .map(|values|
             values.map(|s| FromStr::from_str(s).unwrap())
             .collect::<Vec<_>>()
        ).unwrap_or_else(|| vec![]);
    let usernames = matches.is_present("USERNAMES");
    pretty_env_logger::init();

    let mut rt = Runtime::new().unwrap();

    let (value_tx, value_rx) = mpsc::unbounded();
    let addr = if ipv4 {
        ([0, 0, 0, 0], port).into()
    } else {
        ([0, 0, 0, 0, 0, 0, 0, 0], port).into()
    };
    let srv = webserver::start(addr, header_checks, value_tx);

    let (client, mut agent) = xmpp::XMPPBot::new(jid, password, muc);

    let forwarder = value_rx.for_each(|wh| {
        match format_webhook(&wh, usernames) {
            Some(text) => {
                agent.send_room_text(text);
            }
            _ => {
                println!("Unhandled webhook payload: {:?}", wh);
            }
        }
        Ok(())
    });

    match rt.block_on(
        client
            .select2(srv)
            .select2(forwarder)
            .map(|_| ())
            .map_err(|_| ())
    ) {
        Ok(_) => (),
        Err(err) => println!("FOO: err: {:?}", err),
    }
}
